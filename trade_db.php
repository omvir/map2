<?php include_once 'reportCalculation.php'; ?>
<?php

include_once('includes/basepath.php');
if(isset($_POST["frmPosted"]) && trim($_POST["frmPosted"]) == "1")
{
  // $partyId = 0;
  $partyId = $_POST['partyId'];
  // if(isset($_POST["partyId"]) && trim($_POST["partyId"]) != "" && trim($_POST["partyId"]) != "0")
  // {
  //   $partyId = trim($_POST["partyId"]);
  //   mysql_query("UPDATE party SET partyName='".trim($_POST["partyName"])."',phoneNo='".trim($_POST["phoneNo"])."' WHERE partyId=".$partyId) or print(mysql_error());
  // }
  // else
  // {
  //   if(isset($_POST["partyName"]) && trim($_POST["partyName"]) != "")
  //   {
  //     mysql_query("INSERT INTO party (partyName,phoneNo) VALUES ('".trim($_POST["partyName"])."','".trim($_POST["phoneNo"])."')") or print(mysql_error());
  //     $partyId = mysql_insert_id();
  //   }
  // }

  if(trim($_POST["tradedateDay"]) != "" && trim($_POST["tradedateMonth"]) != "" && trim($_POST["tradedateYear"]) != "")
  {
    $tradeDateTime = trim($_POST["tradedateYear"])."-".trim($_POST["tradedateMonth"])."-".trim($_POST["tradedateDay"])." ".date("H:i:s");
  }
  else
  {
    $tradeDateTime = "0000-00-00 00:00:00";
  }

  if($partyId != 0 && trim($_POST["itemId"]) != 0)
  {
    $itemId    = $_POST["itemId"];
    $tranType  = $_POST["tranType"];
    $parity    = trim($_POST["parity"]) == "" ? 0 : trim($_POST["parity"]);
    $rate      = trim($_POST["rate"]) == "" ? 0 : trim($_POST["rate"]);
    $bill      = $_POST["delivery"] == 1 ? 'Y' : 'N';
    $vat       = trim($_POST["vat"]) == "" ? 0 : trim($_POST["vat"]);
    $unfixRate = trim($_POST["unfixRate"]) == "" ? 0 : trim($_POST["unfixRate"]);
    
    $parityQty = trim($_POST["parityQty"]) == "" ? 0 : trim($_POST["parityQty"]);
    $rateQty = trim($_POST["rateQty"]) == "" ? 0 : trim($_POST["rateQty"]);
    $billQty = trim($_POST["billQty"]) == "" ? 0 : trim($_POST["billQty"]);
		
		$rc = new reportCalculation();
		
		if(!empty($_POST["parity"]) && $parityQty > 0){
      $tradeFor = "Parity";
	    $sSQL = "INSERT INTO trade (tradeDateTime,tradeFor,partyId,itemId,tranType,qty,parity,rate,bill,vat,unfixRate) VALUES (";
	    $sSQL .= "'".$tradeDateTime."','".$tradeFor."',".$partyId.",".$itemId.",'".$tranType."',".$parityQty.",".$parity.",0,'N',".$vat.",".$unfixRate.")";
	    mysql_query($sSQL);
	    $latestTradeInsertId = mysql_insert_id();
      
      $parityDate = substr($tradeDateTime,0,10);
      $result = $rc->withParityQty($partyId, $latestTradeInsertId, $parityQty, $parity, $parityDate, $itemId, $tranType);
	  }
	  
	  if(!empty($_POST["rate"]) && $rateQty > 0){
      $tradeFor = "Rate";
	    $sSQL = "INSERT INTO trade (tradeDateTime,tradeFor,partyId,itemId,tranType,qty,parity,rate,bill,vat,unfixRate) VALUES (";
	    $sSQL .= "'".$tradeDateTime."','".$tradeFor."',".$partyId.",".$itemId.",'".$tranType."',".$rateQty.",0,".$rate.",'N',".$vat.",".$unfixRate.")";
	    mysql_query($sSQL);
	    $latestTradeInsertId = mysql_insert_id();
      
      $rateDate = substr($tradeDateTime,0,10);
     	$result = $rc->withRateQty($partyId, $latestTradeInsertId, $rateQty, $rate, $rateDate, $itemId, $tranType);
	  }

    if($bill == 'Y' && $billQty > 0)
    {
      $tradeFor = "Bill";
      $sSQL = "INSERT INTO trade (tradeDateTime,tradeFor,partyId,itemId,tranType,qty,parity,rate,bill,vat,unfixRate) VALUES (";
	    $sSQL .= "'".$tradeDateTime."','".$tradeFor."',".$partyId.",".$itemId.",'".$tranType."',".$billQty.",0,0,'".$bill."',".$vat.",".$unfixRate.")";
	    mysql_query($sSQL);
	    $latestTradeInsertId = mysql_insert_id();
      
      $billDate = substr($tradeDateTime,0,10);
      $result = $rc->withBillQty($partyId, $latestTradeInsertId, $billQty, $vat, $unfixRate, $billDate, $itemId, $tranType);
    }
  }
  $_SESSION['itemId'] = trim($_POST["itemId"]);
  
  if($rc->displayPossibility == 0)
    header("Location:trade.php");
  exit;
}

?>