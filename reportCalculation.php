<?php

//Notes
//=====
//store origParityQty, origRateQty, origBillQty
//For now, ORDER BY tradeMetaId => that should be verified for 1) Edit records and 2) records in old dates
//If rateQty = 0 and billQty != 0 and rate != 0 ... it means rate is unfixRate, so we have NOT created separate unfixRate field in trade table
//partyId  field in trademeta used for insertion, but NOT for update, because trademeta table will have same value for now
//itemId   field in trademeta used for insertion, but NOT for update, because trademeta table will have same value for now
//tranType field in trademeta used for insertion, but NOT for update, because trademeta table will have same value for now
//Work
//=====
//trademeta => 1) created field for insertion, 2) created field can help in order by ?
//parityUsedQty,rateUsedQty,billUsedQty handling => trade table 
//Delete and Edit handling => In delete, for trademeta table, we can just make id = 0 and qty = 0 instead of handling all
//trade_db.php => "DELETE FROM trade WHERE tradeId = '".trim($_REQUEST["entryNo"])."'";
//unfixRate and rate should be different column in trademeta
//order by firstdttime, secnddttime, thirdtim, "tradeMetaId"
//FROM Hardeepbhai :Start
//trade.php
//trade_db.php
//handle_parity.php
//handle_rate.php
//handle_bill.php
//rade-meta.class.php
//trade.class.php
//trade.js
//query.sql
//FROM Hardeepbhai :End
//Hardeepbhai should do:Start
//handle real qty
//handl created properly
//Hardeepbhai should do:End
include_once('includes/basepath.php');

class reportCalculation
{

  public $displayPossibility = 0;

  function withParityQty($partyId, $latestTradeInsertId, $parityQty, $parity, $parityDate, $itemId, $tranType)
  {//withParityQty:Start
    $sourceParityQty = $parityQty;

    $sql = "SELECT * FROM trademeta WHERE parityQtyId = 0 ";
    $cnt = count($this->fetchData($sql));
    $this->echoIfRequired("<br /> initial cnt : ".$cnt);
    if($cnt == 0)
    {//insert all new
      $this->echoIfRequired("<br />Possibility ** 1 ** : <br />");

      $realQty         = $parityQty;
      $parityQtyId     = $latestTradeInsertId;
      $parityDate      = $parityDate;
      $parityQty       = $parityQty;
      $parity          = $parity;
      //Insert default 0 / blank :Start
      $rateQtyId       = 0;
      $billQtyId       = 0;
      $rateDate        = "";
      $billDate        = "";
      $rateQty         = 0;
      $billQty         = 0;
      $rate            = 0;
      $vat             = 0;
      $unfixRate             = 0;
      //Insert default 0 / blank :End
      $firstDateTime   = date("Y-m-d H:i:s");
      $secondDateTime  = "0000-00-00 00:00:00";
      $thirdDateTime   = "0000-00-00 00:00:00";
      $firstThing      = "Parity";
      $sourceParityQty = $sourceParityQty;
      $secondThing     = "";
      $thirdThing      = "";
      $sourceRateQty   = 0;
      $sourceBillQty   = 0;
      $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
    }
    else
    {
      //FOR parityQtyId = 0 AND  (rateQtyId != 0 OR billQtyId != 0) :Start
      $pendingParityQty = $parityQty;

      $sql2                  = "SELECT * FROM trademeta WHERE parityQtyId = 0 AND (rateQtyId != 0 OR billQtyId != 0)"
        ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
      $pRes2                 = mysql_query($sql2);
      $foundQtyWithoutParity = 0;
      while($pRow2                 = mysql_fetch_array($pRes2))
      {
        $foundQtyWithoutParity += $pRow2['realQty'];
      }

      if($foundQtyWithoutParity == $pendingParityQty)
      {//update all found 
        $this->echoIfRequired("<br />Possibility ** 2 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE parityQtyId = 0 AND (rateQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $parityQtyId = $latestTradeInsertId;
          $parityQty   = $pRow2['realQty']; //we want to write same qty as realQty
          $parityDate  = $parityDate;
          $parity      = $parity;
          $pendingParityQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $rateQtyId   = $pRow2['rateQtyId'];
          $billQtyId   = $pRow2['billQtyId'];
          $rateQty     = $pRow2['rateQty'];
          $billQty     = $pRow2['billQty'];
          $rateDate    = $pRow2['rateDate'];
          $billDate    = $pRow2['billDate'];
          $rate        = $pRow2['rate'];
          $vat         = $pRow2['vat'];
          $unfixRate         = $pRow2['unfixRate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceParityQty = $sourceParityQty;
          $sourceRateQty   = $pRow2['sourceRateQty'];
          $sourceBillQty   = $pRow2['sourceBillQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Parity";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Parity";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see that pendingParityQty is 0 => ".$pendingParityQty);
        }
      }
      else if($foundQtyWithoutParity < $pendingParityQty)
      {//update all found, insert remaining
        $this->echoIfRequired("<br />Possibility ** 3 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE parityQtyId = 0 AND (rateQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $parityQtyId = $latestTradeInsertId;
          $parityQty   = $pRow2['realQty']; //we want to write same qty in parity as realQty
          $parityDate  = $parityDate;
          $parity      = $parity;
          $pendingParityQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $rateQtyId   = $pRow2['rateQtyId'];
          $billQtyId   = $pRow2['billQtyId'];
          $rateQty     = $pRow2['rateQty'];
          $billQty     = $pRow2['billQty'];
          $rateDate    = $pRow2['rateDate'];
          $billDate    = $pRow2['billDate'];
          $rate        = $pRow2['rate'];
          $vat         = $pRow2['vat'];
          $unfixRate         = $pRow2['unfixRate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceParityQty = $sourceParityQty;
          $sourceRateQty   = $pRow2['sourceRateQty'];
          $sourceBillQty   = $pRow2['sourceBillQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Parity";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Parity";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much pendingParityQty is => ".$pendingParityQty);
        }
        $this->echoIfRequired("<br />You can see how much pendingParityQty is to be inserted => ".$pendingParityQty);
        // To insert pending:Start //////////////////////////////////////
        //EXCEPT default 0 / blank :End
        $realQty          = $pendingParityQty;
        $parityQtyId      = $latestTradeInsertId;
        $parityDate       = $parityDate;
        $parityQty        = $pendingParityQty;
        $parity           = $parity;
        $pendingParityQty = 0; //because all pending we have inserted
        //EXCEPT default 0 / blank :End
        //default 0 / blank :Start
        $rateQtyId        = 0;
        $billQtyId        = 0;
        $rateDate         = "";
        $billDate         = "";
        $rateQty          = 0;
        $billQty          = 0;
        $rate             = 0;
        $vat              = 0;
        $unfixRate              = 0;
        //default 0 / blank :End
        $firstDateTime    = date("Y-m-d H:i:s");
        $secondDateTime   = "0000-00-00 00:00:00";
        $thirdDateTime    = "0000-00-00 00:00:00";
        $firstThing       = "Parity";
        $sourceParityQty  = $sourceParityQty;
        $secondThing      = "";
        $thirdThing       = "";
        $sourceRateQty    = 0;
        $sourceBillQty    = 0;
        $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
        $this->echoIfRequired("<br />You can see how much pendingParityQty is => ".$pendingParityQty);
        // To insert pending:End //////////////////////////////////////
      }
      else if($foundQtyWithoutParity > $pendingParityQty)
      {
        $this->echoIfRequired("<br />Possibility ** 4 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE parityQtyId = 0 AND (rateQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          $pendingOldRealQtyForCurrentRow = $pRow2['realQty']; //We may require to split current row's qty
          //Something changed from current found table row:Start
          $parityQtyId                    = $latestTradeInsertId;
          $parityDate                     = $parityDate;
          $parity                         = $parity;
          if($pRow2['realQty'] == $pendingParityQty)
          {
            $this->echoIfRequired("<br />Possibility ** 4.1 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['realQty'];
            $rateQty                        = $pRow2['rateQty'];
            $billQty                        = $pRow2['billQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingParityQty               = 0;
          }
          else if($pRow2['realQty'] < $pendingParityQty)
          {//Though $foundQtyWithoutParity is greater, but it may possible that current row has less
            $this->echoIfRequired("<br />Possibility ** 4.2 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['realQty'];
            $rateQty                        = $pRow2['rateQty'];
            $billQty                        = $pRow2['billQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingParityQty               = $pendingParityQty - $pRow2['realQty']; //When while loop completed, $pendingParityQty will be 0, because we are in if condition of $foundQtyWithoutParity > $pendingParityQty
          }
          else if($pRow2['realQty'] > $pendingParityQty)
          {
            $this->echoIfRequired("<br />Possibility ** 4.3 ** : <br />");
            $realQty   = $pendingParityQty;
            $parityQty = $pendingParityQty;
            if($pRow2['rateQty'] != 0)
              $rateQty   = $pendingParityQty;
            else
              $rateQty   = 0;

            if($pRow2['billQty'] != 0)
              $billQty = $pendingParityQty;
            else
              $billQty = 0;

            $pendingOldRealQtyForCurrentRow = $pRow2['realQty'] - $pendingParityQty;
            $pendingParityQty               = 0;
          }
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId   = $pRow2['tradeMetaId'];
          $rateQtyId     = $pRow2['rateQtyId'];
          $billQtyId     = $pRow2['billQtyId'];
          $rateDate      = $pRow2['rateDate'];
          $billDate      = $pRow2['billDate'];
          $rate          = $pRow2['rate'];
          $vat           = $pRow2['vat'];
          $unfixRate           = $pRow2['unfixRate'];
          $firstDateTime = $pRow2['firstDateTime'];
          $firstThing    = $pRow2['firstThing'];
          $sourceRateQty = $pRow2['sourceRateQty'];
          $sourceBillQty = $pRow2['sourceBillQty'];
          //NOT changed from current found table row:End

          $sourceParityQty = $sourceParityQty;
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Parity";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Parity";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much <b>pending Qty ForCurrentRow</b> is to be inserted => ".$pendingOldRealQtyForCurrentRow);

          if($pendingOldRealQtyForCurrentRow > 0)
          {
            // To insert pending:Start //////////////////////////////////////
            $realQty = $pendingOldRealQtyForCurrentRow;
            if($pRow2['rateQty'] != 0)
              $rateQty = $pendingOldRealQtyForCurrentRow;
            else
              $rateQty = 0;

            if($pRow2['billQty'] != 0)
              $billQty = $pendingOldRealQtyForCurrentRow;
            else
              $billQty = 0;

            $rateQtyId = $pRow2['rateQtyId'];
            $rateDate  = $pRow2['rateDate'];
            $rate      = $pRow2['rate'];
            $billQtyId = $pRow2['billQtyId'];
            $billDate  = $pRow2['billDate'];
            $vat       = $pRow2['vat'];
            $unfixRate       = $pRow2['unfixRate'];

            $parityQtyId     = 0;
            $parityDate      = "";
            $parityQty       = 0;
            $parity          = 0;
            $thirdDateTime   = "0000-00-00 00:00:00";
            $thirdThing      = "";
            $sourceParityQty = 0;

//We are inserting same row as current row (qty changed, which is handled above) :Start
            $firstDateTime  = $pRow2['firstDateTime'];
            $secondDateTime = $pRow2['secondDateTime'];
            $firstThing     = $pRow2['firstThing'];
            $secondThing    = $pRow2['secondThing'];
            $sourceRateQty  = $pRow2['sourceRateQty'];
            $sourceBillQty  = $pRow2['sourceBillQty'];
//We are inserting same row as current row (qty changed, which is handled above) :End

            $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
            //$pendingParityQty will not updated because this is pending pendingOldRealQtyForCurrentRow insertion
            $this->echoIfRequired("<br />You can see how much pendingOldRealQtyForCurrentRow is => ".$pendingOldRealQtyForCurrentRow);
          }

          if($pendingParityQty == 0)
            break;
        }
      }
    }
  }

  function withRateQty($partyId, $latestTradeInsertId, $rateQty, $rate, $rateDate, $itemId, $tranType)
  {//withRateQty:Start
    $sourceRateQty = $rateQty;

    $sql = "SELECT * FROM trademeta WHERE rateQtyId = 0 ";
    $cnt = count($this->fetchData($sql));
    $this->echoIfRequired("<br /> RATE initial cnt : ".$cnt);
    if($cnt == 0)
    {//insert all new
      $this->echoIfRequired("<br />Possibility ** 11 ** : <br />");

      $realQty         = $rateQty;
      $rateQtyId       = $latestTradeInsertId;
      $rateDate        = $rateDate;
      $rateQty         = $rateQty;
      $rate            = $rate;
      //Insert default 0 / blank :Start
      $parityQtyId     = 0;
      $billQtyId       = 0;
      $parityDate      = "";
      $billDate        = "";
      $parityQty       = 0;
      $billQty         = 0;
      $parity          = 0;
      $vat             = 0;
      $unfixRate             = 0;
      //Insert default 0 / blank :End
      $firstDateTime   = date("Y-m-d H:i:s");
      $secondDateTime  = "0000-00-00 00:00:00";
      $thirdDateTime   = "0000-00-00 00:00:00";
      $firstThing      = "Rate";
      $sourceRateQty   = $sourceRateQty;
      $secondThing     = "";
      $thirdThing      = "";
      $sourceParityQty = 0;
      $sourceBillQty   = 0;
      $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
    }
    else
    {
      //FOR rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0) :Start
      $pendingRateQty = $rateQty;

      $sql2                = "SELECT * FROM trademeta WHERE rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0)"
        ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
      $pRes2               = mysql_query($sql2);
      $foundQtyWithoutRate = 0;
      while($pRow2               = mysql_fetch_array($pRes2))
      {
        $foundQtyWithoutRate += $pRow2['realQty'];
      }

      if($foundQtyWithoutRate == $pendingRateQty)
      {//update all found 
        $this->echoIfRequired("<br />Possibility ** 22 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $rateQtyId   = $latestTradeInsertId;
          $rateQty     = $pRow2['realQty']; //we want to write same qty as realQty
          $rateDate    = $rateDate;
          $rate        = $rate;
          $pendingRateQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $parityQtyId = $pRow2['parityQtyId'];
          $billQtyId   = $pRow2['billQtyId'];
          $parityQty   = $pRow2['parityQty'];
          $billQty     = $pRow2['billQty'];
          $parityDate  = $pRow2['parityDate'];
          $billDate    = $pRow2['billDate'];
          $parity      = $pRow2['parity'];
          $vat         = $pRow2['vat'];
          $unfixRate         = $pRow2['unfixRate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceRateQty   = $sourceRateQty;
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceBillQty   = $pRow2['sourceBillQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Rate";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Rate";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see that pendingRateQty is 0 => ".$pendingRateQty);
        }
      }
      else if($foundQtyWithoutRate < $pendingRateQty)
      {//update all found, insert remaining
        $this->echoIfRequired("<br />Possibility ** 33 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $rateQtyId   = $latestTradeInsertId;
          $rateQty     = $pRow2['realQty']; //we want to write same qty in parity as realQty
          $rateDate    = $rateDate;
          $rate        = $rate;
          $pendingRateQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $parityQtyId = $pRow2['parityQtyId'];
          $billQtyId   = $pRow2['billQtyId'];
          $parityQty   = $pRow2['parityQty'];
          $billQty     = $pRow2['billQty'];
          $parityDate  = $pRow2['parityDate'];
          $billDate    = $pRow2['billDate'];
          $parity      = $pRow2['parity'];
          $vat         = $pRow2['vat'];
          $unfixRate         = $pRow2['unfixRate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceRateQty   = $sourceRateQty;
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceBillQty   = $pRow2['sourceBillQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = $pRow2['thirdDateTime']; //This will be "0000-00-00 00:00:00"
            $secondThing    = "Rate";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Rate";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much pendingRateQty is => ".$pendingRateQty);
        }
        $this->echoIfRequired("<br />You can see how much pendingRateQty is to be inserted => ".$pendingRateQty);
        // To insert pending:Start //////////////////////////////////////
        //EXCEPT default 0 / blank :End
        $realQty         = $pendingRateQty;
        $rateQtyId       = $latestTradeInsertId;
        $rateDate        = $rateDate;
        $rateQty         = $pendingRateQty;
        $rate            = $rate;
        $pendingRateQty  = 0; //because all pending we have inserted
        //EXCEPT default 0 / blank :End
        //default 0 / blank :Start
        $parityQtyId     = 0;
        $billQtyId       = 0;
        $parityDate      = "";
        $billDate        = "";
        $parityQty       = 0;
        $billQty         = 0;
        $parity          = 0;
        $vat             = 0;
        $unfixRate             = 0;
        //default 0 / blank :End
        $firstDateTime   = date("Y-m-d H:i:s");
        $secondDateTime  = "0000-00-00 00:00:00";
        $thirdDateTime   = "0000-00-00 00:00:00";
        $firstThing      = "Rate";
        $sourceRateQty   = $sourceRateQty;
        $secondThing     = "";
        $thirdThing      = "";
        $sourceParityQty = 0;
        $sourceBillQty   = 0;
        $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
        $this->echoIfRequired("<br />You can see how much pendingRateQty is => ".$pendingRateQty);
        // To insert pending:End //////////////////////////////////////
      }
      else if($foundQtyWithoutRate > $pendingRateQty)
      {
        $this->echoIfRequired("<br />Possibility ** 44 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          $pendingOldRealQtyForCurrentRow = $pRow2['realQty']; //We may require to split current row's qty
          //Something changed from current found table row:Start
          $rateQtyId                      = $latestTradeInsertId;
          $rateDate                       = $rateDate;
          $rate                           = $rate;
          if($pRow2['realQty'] == $pendingRateQty)
          {
            $this->echoIfRequired("<br />Possibility ** 44.1 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $rateQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['parityQty'];
            $billQty                        = $pRow2['billQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingRateQty                 = 0;
          }
          else if($pRow2['realQty'] < $pendingRateQty)
          {//Though $foundQtyWithoutRate is greater, but it may possible that current row has less
            $this->echoIfRequired("<br />Possibility ** 44.2 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $rateQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['parityQty'];
            $billQty                        = $pRow2['billQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingRateQty                 = $pendingRateQty - $pRow2['realQty']; //When while loop completed, $pendingRateQty will be 0, because we are in if condition of $foundQtyWithoutRate > $pendingRateQty
          }
          else if($pRow2['realQty'] > $pendingRateQty)
          {
            $this->echoIfRequired("<br />Possibility ** 44.3 ** : <br />");
            $realQty   = $pendingRateQty;
            $rateQty   = $pendingRateQty;
            if($pRow2['parityQty'] != 0)
              $parityQty = $pendingRateQty;
            else
              $parityQty = 0;

            if($pRow2['billQty'] != 0)
              $billQty = $pendingRateQty;
            else
              $billQty = 0;

            $pendingOldRealQtyForCurrentRow = $pRow2['realQty'] - $pendingRateQty;
            $pendingRateQty                 = 0;
          }
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId     = $pRow2['tradeMetaId'];
          $parityQtyId     = $pRow2['parityQtyId'];
          $billQtyId       = $pRow2['billQtyId'];
          $parityDate      = $pRow2['parityDate'];
          $billDate        = $pRow2['billDate'];
          $parity          = $pRow2['parity'];
          $vat             = $pRow2['vat'];
          $unfixRate             = $pRow2['unfixRate'];
          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceBillQty   = $pRow2['sourceBillQty'];
          //NOT changed from current found table row:End

          $sourceRateQty = $sourceRateQty;
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = $pRow2['thirdDateTime']; //This will be "0000-00-00 00:00:00"
            $secondThing    = "Rate";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Rate";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much <b>pending Qty ForCurrentRow</b> is to be inserted => ".$pendingOldRealQtyForCurrentRow);

          if($pendingOldRealQtyForCurrentRow > 0)
          {
            // To insert pending:Start //////////////////////////////////////
            $realQty   = $pendingOldRealQtyForCurrentRow;
            if($pRow2['parityQty'] != 0)
              $parityQty = $pendingOldRealQtyForCurrentRow;
            else
              $parityQty = 0;

            if($pRow2['billQty'] != 0)
              $billQty = $pendingOldRealQtyForCurrentRow;
            else
              $billQty = 0;

            $parityQtyId = $pRow2['parityQtyId'];
            $parityDate  = $pRow2['parityDate'];
            $parity      = $pRow2['parity'];
            $billQtyId   = $pRow2['billQtyId'];
            $billDate    = $pRow2['billDate'];
            $vat         = $pRow2['vat'];
            $unfixRate         = $pRow2['unfixRate'];

            $rateQtyId     = 0;
            $rateDate      = "";
            $rateQty       = 0;
            $rate          = 0;
            $sourceRateQty = 0;

//We are inserting same row as current row (qty changed, which is handled above) :Start
            $firstDateTime   = $pRow2['firstDateTime'];
            $secondDateTime  = $pRow2['secondDateTime'];
            $thirdDateTime   = $pRow2['thirdDateTime'];
            $firstThing      = $pRow2['firstThing'];
            $secondThing     = $pRow2['secondThing'];
            $thirdThing      = $pRow2['thirdThing'];
            $sourceParityQty = $pRow2['sourceParityQty'];
            $sourceBillQty   = $pRow2['sourceBillQty'];
//We are inserting same row as current row (qty changed, which is handled above) :End

            $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
            //$pendingRateQty will not updated because this is pendingOldRealQtyForCurrentRow insertion
            $this->echoIfRequired("<br />You can see how much pendingOldRealQtyForCurrentRow is => ".$pendingOldRealQtyForCurrentRow);
          }

          if($pendingRateQty == 0)
            break;
        }
      }
    }
  }

  function withBillQty($partyId, $latestTradeInsertId, $billQty, $vat, $unfixRate, $billDate, $itemId, $tranType)
  {//withBillQty:Start
    $sourceBillQty = $billQty;

    $sql = "SELECT * FROM trademeta WHERE billQtyId = 0 ";
    $cnt = count($this->fetchData($sql));
    $this->echoIfRequired("<br /> initial cnt : ".$cnt);
    if($cnt == 0)
    {//insert all new
      $this->echoIfRequired("<br />Possibility ** 111 ** : <br />");

      $realQty         = $billQty;
      $billQtyId       = $latestTradeInsertId;
      $billDate        = $billDate;
      $billQty         = $billQty;
      $vat             = $vat;
      $unfixRate       = $unfixRate;
      //Insert default 0 / blank :Start
      $parityQtyId     = 0;
      $rateQtyId       = 0;
      $parityDate      = "";
      $rateDate        = "";
      $parityQty       = 0;
      $rateQty         = 0;
      $parity          = 0;
      $rate            = 0;
      //Insert default 0 / blank :End
      $firstDateTime   = date("Y-m-d H:i:s");
      $secondDateTime  = "0000-00-00 00:00:00";
      $thirdDateTime   = "0000-00-00 00:00:00";
      $firstThing      = "Bill";
      $sourceBillQty   = $sourceBillQty;
      $secondThing     = "";
      $thirdThing      = "";
      $sourceParityQty = 0;
      $sourceRateQty   = 0;
      $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
    }
    else
    {
      //FOR rateQtyId = 0 AND (parityQtyId != 0 OR billQtyId != 0) :Start
      $pendingBillQty = $billQty;

      $sql2                = "SELECT * FROM trademeta WHERE billQtyId = 0 AND (parityQtyId != 0 OR rateQtyId != 0)"
        ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
      $pRes2               = mysql_query($sql2);
      $foundQtyWithoutBill = 0;
      while($pRow2               = mysql_fetch_array($pRes2))
      {
        $foundQtyWithoutBill += $pRow2['realQty'];
      }

      if($foundQtyWithoutBill == $pendingBillQty)
      {//update all found 
        $this->echoIfRequired("<br />Possibility ** 222 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE billQtyId = 0 AND (parityQtyId != 0 OR rateQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $billQtyId   = $latestTradeInsertId;
          $billQty     = $pRow2['realQty']; //we want to write same qty as realQty
          $billDate    = $billDate;
          $vat         = $vat;
          $unfixRate   = $unfixRate;
          $pendingBillQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $parityQtyId = $pRow2['parityQtyId'];
          $rateQtyId   = $pRow2['rateQtyId'];
          $parityQty   = $pRow2['parityQty'];
          $rateQty     = $pRow2['rateQty'];
          $parityDate  = $pRow2['parityDate'];
          $rateDate    = $pRow2['rateDate'];
          $parity      = $pRow2['parity'];
          $rate        = $pRow2['rate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceBillQty   = $sourceBillQty;
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceRateQty   = $pRow2['sourceRateQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Bill";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Bill";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see that pendingBillQty is 0 => ".$pendingBillQty);
        }
      }
      else if($foundQtyWithoutBill < $pendingBillQty)
      {//update all found, insert remaining
        $this->echoIfRequired("<br />Possibility ** 333 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE billQtyId = 0 AND (parityQtyId != 0 OR rateQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          //Something changed from current found table row:Start
          $billQtyId   = $latestTradeInsertId;
          $billQty     = $pRow2['realQty']; //we want to write same qty in parity as realQty
          $billDate    = $billDate;
          $vat         = $vat;
          $unfixRate         = $unfixRate;
          $pendingBillQty -= $pRow2['realQty'];
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId = $pRow2['tradeMetaId'];
          $realQty     = $pRow2['realQty'];
          $parityQtyId = $pRow2['parityQtyId'];
          $rateQtyId   = $pRow2['rateQtyId'];
          $parityQty   = $pRow2['parityQty'];
          $rateQty     = $pRow2['rateQty'];
          $parityDate  = $pRow2['parityDate'];
          $rateDate    = $pRow2['rateDate'];
          $parity      = $pRow2['parity'];
          $rate        = $pRow2['rate'];
          //NOT changed from current found table row:End

          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceBillQty   = $sourceBillQty;
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceRateQty   = $pRow2['sourceRateQty'];
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Bill";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Bill";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much pendingBillQty is => ".$pendingBillQty);
        }
        $this->echoIfRequired("<br />You can see how much pendingBillQty is to be inserted => ".$pendingBillQty);
        // To insert pending:Start //////////////////////////////////////
        //EXCEPT default 0 / blank :End
        $realQty         = $pendingBillQty;
        $billQtyId       = $latestTradeInsertId;
        $billDate        = $billDate;
        $billQty         = $pendingBillQty;
        $vat             = $vat;
        $unfixRate             = $unfixRate;
        $pendingBillQty  = 0; //because all pending we have inserted
        //EXCEPT default 0 / blank :End
        //default 0 / blank :Start
        $parityQtyId     = 0;
        $rateQtyId       = 0;
        $parityDate      = "";
        $rateDate        = "";
        $parityQty       = 0;
        $rateQty         = 0;
        $parity          = 0;
        $rate            = 0;
        //default 0 / blank :End
        $firstDateTime   = date("Y-m-d H:i:s");
        $secondDateTime  = "0000-00-00 00:00:00";
        $thirdDateTime   = "0000-00-00 00:00:00";
        $firstThing      = "Bill";
        $sourceBillQty   = $sourceBillQty;
        $secondThing     = "";
        $thirdThing      = "";
        $sourceParityQty = 0;
        $sourceRateQty   = 0;
        $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
        $this->echoIfRequired("<br />You can see how much pendingBillQty is => ".$pendingBillQty);
        // To insert pending:End //////////////////////////////////////
      }
      else if($foundQtyWithoutBill > $pendingBillQty)
      {
        $this->echoIfRequired("<br />Possibility ** 444 ** : <br />");

        $sql2  = "SELECT * FROM trademeta WHERE billQtyId = 0 AND (parityQtyId != 0 OR rateQtyId != 0)"
          ." ORDER BY firstDateTime, secondDateTime, thirdDateTime";
        $pRes2 = mysql_query($sql2);

        while($pRow2 = mysql_fetch_array($pRes2))
        {
          $pendingOldRealQtyForCurrentRow = $pRow2['realQty']; //We may require to split current row's qty
          //Something changed from current found table row:Start
          $billQtyId                      = $latestTradeInsertId;
          $billDate                       = $billDate;
          $vat                            = $vat;
          $unfixRate                            = $unfixRate;
          if($pRow2['realQty'] == $pendingBillQty)
          {
            $this->echoIfRequired("<br />Possibility ** 444.1 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $billQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['parityQty'];
            $rateQty                        = $pRow2['rateQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingBillQty                 = 0;
          }
          else if($pRow2['realQty'] < $pendingBillQty)
          {//Though $foundQtyWithoutBill is greater, but it may possible that current row has less
            $this->echoIfRequired("<br />Possibility ** 444.2 ** : <br />");
            $realQty                        = $pRow2['realQty'];
            $billQty                        = $pRow2['realQty'];
            $parityQty                      = $pRow2['parityQty'];
            $rateQty                        = $pRow2['rateQty'];
            $pendingOldRealQtyForCurrentRow = 0;
            $pendingBillQty                 = $pendingBillQty - $pRow2['realQty']; //When while loop completed, $pendingBillQty will be 0, because we are in if condition of $foundQtyWithoutBill > $pendingBillQty
          }
          else if($pRow2['realQty'] > $pendingBillQty)
          {
            $this->echoIfRequired("<br />Possibility ** 444.3 ** : <br />");
            $realQty   = $pendingBillQty;
            $billQty   = $pendingBillQty;
            if($pRow2['parityQty'] != 0)
              $parityQty = $pendingBillQty;
            else
              $parityQty = 0;

            if($pRow2['rateQty'] != 0)
              $rateQty = $pendingBillQty;
            else
              $rateQty = 0;

            $pendingOldRealQtyForCurrentRow = $pRow2['realQty'] - $pendingBillQty;
            $pendingBillQty                 = 0;
          }
          //Something changed from current found table row:End
          //NOT changed from current found table row:Start
          $tradeMetaId     = $pRow2['tradeMetaId'];
          $parityQtyId     = $pRow2['parityQtyId'];
          $rateQtyId       = $pRow2['rateQtyId'];
          $parityDate      = $pRow2['parityDate'];
          $rateDate        = $pRow2['rateDate'];
          $parity          = $pRow2['parity'];
          $rate            = $pRow2['rate'];
          $firstDateTime   = $pRow2['firstDateTime'];
          $firstThing      = $pRow2['firstThing'];
          $sourceParityQty = $pRow2['sourceParityQty'];
          $sourceRateQty   = $pRow2['sourceRateQty'];
          //NOT changed from current found table row:End

          $sourceBillQty = $sourceBillQty;
          if($pRow2['secondDateTime'] == "0000-00-00 00:00:00")
          {
            $secondDateTime = date("Y-m-d H:i:s");
            $thirdDateTime  = "0000-00-00 00:00:00";
            $secondThing    = "Bill";
            $thirdThing     = "";
          }
          else
          {
            $secondDateTime = $pRow2['secondDateTime'];
            $thirdDateTime  = date("Y-m-d H:i:s");
            $secondThing    = $pRow2['secondThing'];
            $thirdThing     = "Bill";
          }

          $this->upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate);
          $this->echoIfRequired("<br />You can see how much <b>pending Qty ForCurrentRow</b> is to be inserted => ".$pendingOldRealQtyForCurrentRow);

          if($pendingOldRealQtyForCurrentRow > 0)
          {
            // To insert pending:Start //////////////////////////////////////
            $realQty   = $pendingOldRealQtyForCurrentRow;
            if($pRow2['parityQty'] != 0)
              $parityQty = $pendingOldRealQtyForCurrentRow;
            else
              $parityQty = 0;

            if($pRow2['rateQty'] != 0)
              $rateQty = $pendingOldRealQtyForCurrentRow;
            else
              $rateQty = 0;

            $parityQtyId = $pRow2['parityQtyId'];
            $parityDate  = $pRow2['parityDate'];
            $parity      = $pRow2['parity'];
            $rateQtyId   = $pRow2['rateQtyId'];
            $rateDate    = $pRow2['rateDate'];
            $rate        = $pRow2['rate'];

            $billQtyId     = 0;
            $billDate      = "";
            $billQty       = 0;
            $vat           = 0;
            $unfixRate           = 0;
            $sourceBillQty = 0;

//We are inserting same row as current row (qty changed, which is handled above) :Start
            $firstDateTime   = $pRow2['firstDateTime'];
            $secondDateTime  = $pRow2['secondDateTime'];
            $thirdDateTime   = $pRow2['thirdDateTime'];
            $firstThing      = $pRow2['firstThing'];
            $secondThing     = $pRow2['secondThing'];
            $thirdThing      = $pRow2['thirdThing'];
            $sourceParityQty = $pRow2['sourceParityQty'];
            $sourceRateQty   = $pRow2['sourceRateQty'];
//We are inserting same row as current row (qty changed, which is handled above) :End

            $this->insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType);
            //$pendingBillQty will not updated because this is pendingOldRealQtyForCurrentRow insertion
            $this->echoIfRequired("<br />You can see how much pendingOldRealQtyForCurrentRow is => ".$pendingOldRealQtyForCurrentRow);
          }

          if($pendingBillQty == 0)
            break;
        }
      }
    }
  }

  function fetchData($sql)
  {
    $data    = array();
    $pRes    = mysql_query($sql);
    $num_row = mysql_num_rows($pRes);
    $pRes    = mysql_fetch_array($pRes);
    if($num_row > 0)
    {//Do we need to change this to while loop ???
      $data['tradeMetaId'] = $pRes['tradeMetaId'];
    }
    return $data;
  }

  function insertMetaData($firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $partyId, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate, $itemId, $tranType)
  {
    $mSQL = "INSERT INTO trademeta (firstDateTime, secondDateTime, thirdDateTime, firstThing, secondThing, thirdThing, partyId, realQty, 
      parityQtyId, rateQtyId, billQtyId, parityDate, rateDate, billDate, parityQty, rateQty, billQty, sourceParityQty, sourceRateQty, sourceBillQty, 
      parity, rate, vat, unfixRate, itemId, tranType)
      VALUES ('".$firstDateTime."', '".$secondDateTime."', '".$thirdDateTime."', '".$firstThing."', '".$secondThing."', '".$thirdThing."', ".
      $partyId.", ".$realQty.", ".$parityQtyId.", ".$rateQtyId.", ".$billQtyId.", '".$parityDate."', '".$rateDate."', '".$billDate."', ".
      $parityQty.", ".$rateQty.", ".$billQty.", ".$sourceParityQty.", ".$sourceRateQty.", ".$sourceBillQty.", ".
      $parity.", ".$rate.", ".$vat.", ".$unfixRate.", ".$itemId.", '".$tranType."'
      )";
    mysql_query($mSQL);
  }

  function upDateTredeMeta($tradeMetaId, $firstDateTime, $secondDateTime, $thirdDateTime, $firstThing, $secondThing, $thirdThing, $realQty, $parityQtyId, $rateQtyId, $billQtyId, $parityDate, $rateDate, $billDate, $parityQty, $rateQty, $billQty, $sourceParityQty, $sourceRateQty, $sourceBillQty, $parity, $rate, $vat, $unfixRate)
  {
    $update2 = "UPDATE trademeta SET firstDateTime = '".$firstDateTime."', secondDateTime = '".$secondDateTime."', thirdDateTime = '".$thirdDateTime."', ".
      " firstThing = '".$firstThing."', secondThing = '".$secondThing."', thirdThing = '".$thirdThing."', ".
      " realQty = ".$realQty.", ".
      " parityQtyId = ".$parityQtyId.", rateQtyId = ".$rateQtyId.", billQtyId = ".$billQtyId.", ".
      " parityDate = '".$parityDate."', rateDate = '".$rateDate."', billDate = '".$billDate."', ".
      " parityQty = ".$parityQty.", rateQty = ".$rateQty.", billQty = ".$billQty.", ".
      " sourceParityQty = ".$sourceParityQty.", sourceRateQty = ".$sourceRateQty.", sourceBillQty = ".$sourceBillQty.", ".
      " parity = ".$parity.", rate = ".$rate.", vat = ".$vat.", unfixRate = ".$unfixRate.
      " WHERE tradeMetaId = ".$tradeMetaId;
    mysql_query($update2);
  }

  function echoIfRequired($theString)
  {
    if($this->displayPossibility == 1)
      echo $theString;
  }

}

?>