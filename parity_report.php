<?php
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');

$partyId = isset($_REQUEST['partyId']) ? $_REQUEST['partyId'] : 0;
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Item</h1>
</section>
<section class="content color-entry">
  <div class="col-xs-12">
    		<div class="box">
        	<div class="box-body table-responsive">
            <table class="table table-bordered table-hover">
            	<thead>
              	<tr>
                  <th>Party Name</th>
                  <th>Date</th>
                  <th>Parity Qty</th>
                  <th>Real Qty</th>
                  <th>Rate Qty</th>
                  <th>Bill Qty</th>
                  <th>Rate</th>
                  <th>Parity</th>
                  <th>RateAfterParity</th>
                  <th>Buy/Sell</th>
                  <th>Item</th>
                </tr>
              </thead>
              <tbody>
				<?php
        $totalParityQty    = 0;
        $parityQtyWithRate = 0;
        $parityQtyWithBill = 0;
				$totalAmountAfterParity = 0;
				$qrySelTradeMeta = "SELECT tm.*,p.*, t.tranType,i.itemName, SUM(tm.parityQty) as totalParity "
          . "  FROM trademeta tm LEFT JOIN party p ON tm.partyId = p.partyId "
          . "  LEFT JOIN trade t ON tm.parityQtyId = t.tradeId "
          . "  LEFT JOIN item i ON t.itemId = i.itemId "
          . " WHERE tm.partyId = ".$partyId
          . " GROUP BY tm.parityQtyId HAVING tm.parityQtyId > 0 "
          . " ORDER BY tm.firstDateTime, tm.secondDateTime, tm.thirdDateTime";
				$resSelTradeMeta = mysql_query($qrySelTradeMeta);
				if(mysql_num_rows($resSelTradeMeta)>0)
				{
          $parityRowCount = 0;
					while($qFetchTradeMeta = mysql_fetch_array($resSelTradeMeta))
					{
            $parityRowCount++;
            if($parityRowCount % 2 == 0)
              $rowColor = "";
            else
              $rowColor = "";//Old value : pink (But on mouser over it creates problem, so removed)
            $totalParityQty    += $qFetchTradeMeta["totalParity"];
            $parityQtyWithRate += $qFetchTradeMeta["rateQty"];
            $parityQtyWithBill += $qFetchTradeMeta["billQty"];
						?>
                <tr style="background-color: <?php echo $rowColor; ?>">
							<td><?php 
								echo date('d/m/Y',strtotime($qFetchTradeMeta["parityDate"]));
                echo "<br />".$qFetchTradeMeta["partyName"].'<br/>Phone: '.$qFetchTradeMeta["phoneNo"]; ?></td>
							<td nowrap>
                <?php
                  echo $qFetchTradeMeta["firstDateTime"]." ".$qFetchTradeMeta["firstThing"];
                  echo "<br />".$qFetchTradeMeta["secondDateTime"]." ".$qFetchTradeMeta["secondThing"];
                  echo "<br />".$qFetchTradeMeta["thirdDateTime"]." ".$qFetchTradeMeta["thirdThing"];
                ?>
              </td>
							<td align="right">
                <?php
                  echo $qFetchTradeMeta["totalParity"];//Not displayed : $qFetchTradeMeta["parityDate"]
                ?>
              </td>
							<td align="right"><?php echo $qFetchTradeMeta["realQty"]; ?></td>
							<td align="right">
                <?php
                  if($qFetchTradeMeta["rateQty"] != 0)
                    echo $qFetchTradeMeta["rateQty"]." / ".$qFetchTradeMeta["sourceRateQty"];//Not displayed : $qFetchTradeMeta["rateDate"]
                  else
                    echo "&nbsp;";
                ?>
              </td>
							<td align="right">
                <?php
                  if($qFetchTradeMeta["billQty"] != 0)
                    echo $qFetchTradeMeta["billQty"]." / ".$qFetchTradeMeta["sourceBillQty"];//Note displayed : $qFetchTradeMeta["billDate"]
                  else
                    echo "&nbsp;";
                ?>
              </td>
							<td align="right"><?php echo $qFetchTradeMeta["rate"];//."<br />Amount: ".($qFetchTradeMeta["rate"]*$qFetchTradeMeta["billQty"]); ?></td>
              <td align="right"><?php echo $qFetchTradeMeta["parity"]; ?></td>
							<td align="right">
                <?php 
                  if($qFetchTradeMeta["rate"] != 0)
                  {
                    echo $qFetchTradeMeta["rate"] + $qFetchTradeMeta["parity"]."<br />".($qFetchTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchTradeMeta["billQty"];
										$totalAmountAfterParity += ($qFetchTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchTradeMeta["billQty"];
                  }
                  else
                  {
                    echo 0;
                  }
                ?>
              </td>
							<td><?php echo $qFetchTradeMeta["tranType"]; ?></td>
							<td><?php echo $qFetchTradeMeta["itemName"]; ?></td>
						</tr>
						<?php
						$qrySelRemainTradeMeta = "SELECT * FROM trademeta "
              . " WHERE tradeMetaId!='".$qFetchTradeMeta["tradeMetaId"]."' "
              . "   AND partyId = ".$partyId
              . "   AND parityQtyId='".$qFetchTradeMeta["parityQtyId"]."' "
              . " ORDER BY firstDateTime, secondDateTime, thirdDateTime";
						$resSelRemainTradeMeta = mysql_query($qrySelRemainTradeMeta);
						if(mysql_num_rows($resSelRemainTradeMeta)>0)
						{
							while($qFetchRemainTradeMeta = mysql_fetch_array($resSelRemainTradeMeta))
							{
                $parityQtyWithRate += $qFetchRemainTradeMeta["rateQty"];
                $parityQtyWithBill += $qFetchRemainTradeMeta["billQty"];
								?>
								<tr style="background-color: <?php echo $rowColor; ?>">
									<td>&nbsp;</td>
                  <td nowrap>
                    <?php
                      echo "<br />".$qFetchRemainTradeMeta["firstDateTime"]." ".$qFetchRemainTradeMeta["firstThing"];
                      echo "<br />".$qFetchRemainTradeMeta["secondDateTime"]." ".$qFetchRemainTradeMeta["secondThing"];
                      echo "<br />".$qFetchRemainTradeMeta["thirdDateTime"]." ".$qFetchRemainTradeMeta["thirdThing"];
                    ?>
                  </td>
									<td>&nbsp;</td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["realQty"]; ?></td>
									<td align="right">
                    <?php
                      if($qFetchRemainTradeMeta["rateQty"] != 0)
                        echo $qFetchRemainTradeMeta["rateQty"]." / ".$qFetchRemainTradeMeta["sourceRateQty"];//Not displayed : $qFetchRemainTradeMeta["rateDate"]
                      else
                        echo "&nbsp;";
                    ?>
                  </td>
									<td align="right">
                    <?php
                      if($qFetchRemainTradeMeta["billQty"] != 0)
                        echo $qFetchRemainTradeMeta["billQty"]." / ".$qFetchRemainTradeMeta["sourceBillQty"];//Not displayed : $qFetchRemainTradeMeta["billDate"]
                      else
                        echo "&nbsp;";
                    ?>
                  </td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["rate"];//."<br />".($qFetchRemainTradeMeta["rate"]*$qFetchRemainTradeMeta["billQty"]); ?></td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["parity"]; ?></td>
									<td align="right">
                    <?php
                      if($qFetchRemainTradeMeta["rate"] != 0)
                      {
                        echo $qFetchRemainTradeMeta["rate"] + $qFetchTradeMeta["parity"]."<br />".(($qFetchRemainTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchRemainTradeMeta["billQty"]); 
												$totalAmountAfterParity += ($qFetchRemainTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchRemainTradeMeta["billQty"];
                      }
                      else
                      {
                        echo 0;
                      }
                    ?>
                  </td>
									<td Colspan="2">&nbsp;</td>
								</tr>
								<?php
							}
						}
					}
				}
				else
				{
					?>
					<tr>
						<th colspan="10">No Records Found</th>
					</tr>
					<?php
				}
                ?>
              </tbody>
							<tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>Total</td>
								<td align="right"><?php echo $totalAmountAfterParity; ?></td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
            </table>
            <table border="1" cellpadding="4">
              <tr align="right">
                <td>Total Parity</td>
                <td>Rate Updated For Parity</td>
                <td>Bill Updated For Parity</td>
                <td>Pending Rates For Parity</td>
                <td>Pending Bill For Parity</td>
                <td>Pending Rates For Bill</td>
              </tr>
              <tr align="right">
                <td><?php echo $totalParityQty; ?></td>
                <td><?php echo $parityQtyWithRate; ?></td>
                <td><?php echo $parityQtyWithBill; ?></td>
                <td><?php echo ($totalParityQty - $parityQtyWithRate); ?></td>
                <td><?php echo ($totalParityQty - $parityQtyWithBill); ?></td>
                <td><?php echo ($parityQtyWithRate - $parityQtyWithBill); ?></td>
              </tr>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>

  </div>


</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>item.js" type="text/javascript"></script>
</body></html>