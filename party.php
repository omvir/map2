<?php
	include_once('includes/header.php');
	include_once('includes/topheader.php');
	include_once('includes/leftside.php');
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Party</h1>
</section>
<section class="content color-entry">
  <div class="row">
    <?php include_once('msg.php');?>
    <div class="col-xs-12">
      <form action="party_db.php" method="post" name="color-entry" id="color-entry">
        <input type="hidden" name="frmPosted" value="1">
        <div class="col-md-6 fltLeft">
          <div class="box">
            <div class="box-body">
              <div class="form-group">
                <label>Party Name</label>
                <input type="text" name="partyName" id="partyName" class="form-control">
              </div>
              <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phoneno" id="phoneno" class="form-control onlynum">
              </div>
              <div class="box-footer">
                <input type="submit" name="ok" value="Submit" class="btn btn-primary"/>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  <div class="col-xs-12">
    		<div class="box">
        	<div class="box-body table-responsive">
            <table id="colorList" class="table table-bordered table-hover">
            	<thead>
              	<tr>
                  <th width="50px;">No</th>
                  <th>Party Name</th>
                  <th>Party Phone</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
          	 		<?php
          	 		$sSQL = "SELECT * FROM party ORDER BY partyName";
								$rs = mysql_query($sSQL) or print(mysql_error());
								while($row = mysql_fetch_array($rs)){
          	 		?>
          	 		<tr>
	                <td><?php echo $row["partyId"]; ?></td>
                	<td>
                      <?php echo $row["partyName"]; ?>
                    <a href="parity_report.php?partyId=<?php echo $row["partyId"]; ?>">Parity Report</a> | 
                    <a href="billqty_report.php?partyId=<?php echo $row["partyId"]; ?>">Bill Report</a> | 
                    <a href="rate_report.php?partyId=<?php echo $row["partyId"]; ?>">Rate Report</a>
                  </td>
	                <td><?php echo $row["phoneNo"]; ?></td>
 	                <td>
										<a href="javascript:void(0);" onclick="delalltrade(<?php echo $row["partyId"]; ?>)">Delete Data</a> | 
										<a href="javascript:void(0);" onclick="delitem(<?php echo $row["partyId"]; ?>)">Delete</a>
									</td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
  
  </div>
  
  
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>party.js" type="text/javascript"></script>
</body></html>