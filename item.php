<?php
	include_once('includes/header.php');
	include_once('includes/topheader.php');
	include_once('includes/leftside.php');
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Item</h1>
</section>
<section class="content color-entry">
  <div class="row">
    <?php include_once('msg.php');?>
    <div class="col-xs-12">
      <form action="item_db.php" method="post" name="color-entry" id="color-entry">
        <input type="hidden" name="frmPosted" value="1">
        <div class="col-md-6 fltLeft">
          <div class="box">
            <div class="box-body">
              <div class="form-group">
                <label>Item</label>
                <input type="text" name="itemName" id="itemName" class="form-control">
              </div>
              <div class="form-group">
                <label>Multiply</label>
                <input type="text" name="multiply" id="multiply" class="form-control onlynum">
              </div>
              <div class="box-footer">
                <input type="submit" name="ok" value="Submit" class="btn btn-primary"/>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  <div class="col-xs-12">
    		<div class="box">
        	<div class="box-body table-responsive">
            <table id="colorList" class="table table-bordered table-hover">
            	<thead>
              	<tr>
                  <th width="50px;">No</th>
                  <th>Item</th>
                  <th>Multiply</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
          	 		<?php
          	 		$sSQL = "SELECT * FROM item ORDER BY itemName";
								$rs = mysql_query($sSQL) or print(mysql_error());
								while($row = mysql_fetch_array($rs)){
          	 		?>
          	 		<tr>
	                <td><?php echo $row["itemId"]; ?></td>
	                <td><?php echo $row["itemName"]; ?></td>
	                <td><?php echo $row["multiply"]; ?></td>
	                <td><a href="javascript:void(0);" onclick="delitem(<?php echo $row["itemId"]; ?>)">Delete</a></td>
                </tr>
                <?php
                }
                ?>
              </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
  
  </div>
  
  
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>item.js" type="text/javascript"></script>
</body></html>