<?php
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Item</h1>
</section>
<section class="content color-entry">
  <div class="col-xs-12">
    		<div class="box">
        	<div class="box-body table-responsive">
            <table id="colorList" class="table table-bordered table-hover">
            	<thead>
              	<tr>
                  <th width="50px;">Date</th>
				  <th>Bill Qty ID</th>
				  <th>Parity Qty ID</th>
				  <th>Rate Qty ID</th>
                  <th>Bill Qty</th>
                  <th>Real Qty</th>
                  <th>Parity Qty</th>
				  <th>Rate Qty</th>
				  <th>Parity</th>
				  <th>Rate</th>
                </tr>
              </thead>
              <tbody>
				<?php
				$qrySelTradeMeta = "SELECT * FROM trademeta ORDER BY tradeMetaDate ASC";
				$resSelTradeMeta = mysql_query($qrySelTradeMeta);
				if(mysql_num_rows($resSelTradeMeta)>0)
				{
					while($qFetchTradeMeta = mysql_fetch_array($resSelTradeMeta))
					{
						?>
						<tr>
							<td><?php echo date('d/m/Y',strtotime($qFetchTradeMeta["tradeMetaDate"])); ?></td>
							<td><?php echo $qFetchTradeMeta["billQtyId"]; ?></td>
							<td><?php echo $qFetchTradeMeta["parityQtyId"]; ?></td>
							<td><?php echo $qFetchTradeMeta["rateQtyId"]; ?></td>
							<td><?php echo $qFetchTradeMeta["billQty"]; ?></td>
							<td><?php echo $qFetchTradeMeta["realQty"]; ?></td>
							<td><?php echo $qFetchTradeMeta["parityQty"]; ?></td>
							<td><?php echo $qFetchTradeMeta["rateQty"]; ?></td>
							<td><?php echo $qFetchTradeMeta["parity"]; ?></td>
							<td><?php echo $qFetchTradeMeta["rate"]; ?></td>
						</tr>
						<?php
					}
				}
				else
				{
					?>
					<tr>
						<th colspan="10">No Records Found</th>
					</tr>
					<?php
				}
                ?>
              </tbody>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
  
  </div>
  
  
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>item.js" type="text/javascript"></script>
</body></html>