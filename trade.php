<?php
	include_once('includes/header.php');
	include_once('includes/topheader.php');
	include_once('includes/leftside.php');
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
.day-control { padding:8px 0px; width:64px !important; display:inline; }
.year-control { padding:8px 0px; width:80px !important; display:inline; }
.fourColumn .form-control { width:100px; }
.floatLeft { float:left; width:40%; }
.floatRight { margin-left:40% }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Trade</h1>
</section>
<section class="content color-entry">
  <div class="row">
    <?php include_once('msg.php');?>
    <div class="col-xs-12">
      <form action="trade_db.php" method="post" name="color-entry" id="color-entry">
        <input type="hidden" name="frmPosted" value="1">
        <div class="col-md-6 fltLeft">
          <div class="box">
            <div class="box-body">
              <div class="form-group">
                <label>Phone No.</label>
                <input type="text" name="phoneNo" id="phoneNo" class="form-control">
              </div>
               <div class="form-group">
                <label>Item</label>
                <select class="form-control" name="itemId" id="itemId">
                  <option value="0">Select Item</option>
                  <?php
									$sSQL = "SELECT * FROM item ORDER BY itemName";
									$rs = mysql_query($sSQL) or print(mysql_error());
									while($row = mysql_fetch_array($rs)){
										$seleted ="";
										if($row["itemName"]=='Sliver'){
											$seleted = "selected = seleted";
										}
										
 										echo "<option ".$seleted." value='".$row["itemId"]."' datavalue='".$row["multiply"]."' ".((isset($_SESSION['itemId']) && trim($_SESSION['itemId']) == $row["itemId"]) ? "SELECTED" : "").">".$row["itemName"]."</option>";
									}
									?>
                </select>
              </div>
              
                <div class="form-group">
                <label>Buy/Sell</label>
                <select class="form-control" name="tranType" id="tranType">
                  <option value="Buy">Buy</option>
                  <option value="Sell" selected="selected">Sell</option>
                </select>
              </div>
              <div class="form-group comboLeft">
                <label>Date</label>
                <select name="tradedateDay" id="tradedateDay" class="form-control day-control">
		            		<?php
		            		for($i=1;$i<=31;$i++){
		            			if($i < 10){
		            				echo "<option value='0".$i."'".(($i==date("j"))?" selected ":"").">0".$i."</option>";
		            			}else{
		            				echo "<option value='".$i."'".(($i==date("j"))?" selected ":"").">".$i."</option>";
		            			}
		            		}
		            		?>
		            	</select>&nbsp;<select name="tradedateMonth" id="tradedateMonth" class="form-control day-control" style="width: 10px;">
		            		<?php
		            		for($i=1;$i<=12;$i++){
		            			if($i < 10){
		            				echo "<option value='0".$i."'".(($i==date("n"))?" selected ":"").">0".$i."</option>";
		            			}else{
		            				echo "<option value='".$i."'".(($i==date("n"))?" selected ":"").">".$i."</option>";
		            			}
		            		}
		            		?>
		            	</select>&nbsp;<select name="tradedateYear" id="tradedateYear" class="form-control year-control">
		            		<?php 
							  		for($i=date("Y")-2;$i<=date("Y")+1;$i++){
						  				echo "<option value='".$i."'".(($i==date("Y"))?" selected ":"").">".$i."</option>";
								  	}
								  	?>	
		            	</select>
              </div>
              <div class="form-group">
		            <label>Party</label>
		            <!-- <input type="text" name="partyName" id="partyName" class="form-control fltLeft">
		            <input type="hidden" name="partyId" id="partyId" class="form-control"> -->
                <select name="partyId" class="form-control" autofocus id="partyId">
                  <?php
                    $sql = "SELECT * from party ORDER BY partyName;";
                    $parties = mysql_query($sql);
                    while($row = mysql_fetch_array($parties)){ ?>
                      <option value="<?php echo $row['partyId']; ?>"><?php echo $row['partyName']; ?></option>
                  <?php  }
                  ?>
                </select>
		          </div>
		         </div>
          </div>
				</div>
				<div class="col-md-6 fltLeft">
           <div class="box fourColumn">
            <div class="box-body">
              <div class="form-group floatLeft">
                <label>Parity</label>
                <input type="text" name="parity" id="parity" class="form-control">
              </div>
              <div class="form-group floatRight">
                <label>Parity Qty</label>
                <input type="text" name="parityQty" id="parityQty" class="form-control onlynum">
              </div>
              <div class="form-group floatLeft">
                <label>Rate</label>
                <input type="text" name="rate" id="rate" class="form-control">
              </div>
              <div class="form-group floatRight">
                <label>Rate Qty</label>
                <input type="text" name="rateQty" id="rateQty" class="form-control onlynum">
              </div>
              <div class="form-group floatLeft">
                <label>Delivery Given</label>
                <select class="form-control" name="delivery" id="delivery">
            		  <option value="1">Yes</option>
            		  <option value="0" selected="selected">No</option>
         		   	</select>
              </div>
              <div class="form-group floatRight">
                <label>Bill Qty</label>
                <input type="text" name="billQty" id="billQty" class="form-control onlynum">
              </div>
              <div class="box-footer">
                <input type="submit" name="ok" value="Submit" class="btn btn-primary"/>
              </div>
            </div>
         	</div>
          
          <div class="box">
            <div class="box-body">
	          <div class="form-group">
	                <label>Vat</label>
					 <!-- <select class="form-control" name="vat" id="vat">
				                  <option value="0">VAT</option>
				                  <option value="1">Without VAT</option>
				     	  </select>--> 
   					<input type="text" name="vat" id="vat" value="<?php echo $cfg_defaultVat; ?>" class="form-control onlynum">
              </div>
              <div class="form-group">
                <label>Unfix Rate</label>
                <input type="text" name="unfixRate" id="unfixRate" class="form-control onlynum">
              </div>
              <div class="form-group">
		            <label>Amount</label>
		            <input type="text" name="amount" id="amount" class="form-control onlynum" disabled>
		          </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
  
  <div class="col-xs-12">
		<div class="box">
			<div class="box-body table-responsive">
				<table class="table table-bordered table-hover">
        	<thead>
          	<tr>
              <th width="50px;">No</th>
              <th>Party</th>
              <th>Qty</th>
              <th>Parity</th>
              <th>Rate</th>
              <th>Delivery Given</th>
              <th>Vat</th>
              <th>Amount</th>
              <th>Action</th>
              <th>Item</th>
              <th>Buy/Sell</th>
            </tr>
          </thead>
          <tbody>
      	 		<?php
      	 		$sSQL = "SELECT * FROM trade ORDER BY tradeId DESC";
						$rs = mysql_query($sSQL) or print(mysql_error());
						$i = 1;
						while($row = mysql_fetch_array($rs)){
      	 		?>
      	 		<tr>
              <td align="center"><?php echo $i; ?></td>
              <td><?php echo $row["tradeDateTime"]. " : ". rtnParty($row["partyId"]); ?></td>
              <td align="right"><?php echo $row["qty"]; ?></td>
              <td align="right"><?php echo $row["parity"]; ?></td>
              <td align="right"><?php echo $row["rate"]; ?></td>
               <td align="left"><?php if($row["bill"]=='Y'){echo 'Yes'; }else{echo 'No';} ?></td>
              <td><?php echo $row["vat"];?></td>
              <td align="right"><?php echo $row["rate"]*$row["qty"] ; ?></td>
              <td><a href="javascript:void(0);" onclick="deltrade(<?php echo $row["tradeId"]; ?>)">Delete</a></td>
              <td><?php echo rtnItem($row["itemId"]); ?></td>
              <td><?php echo $row["tranType"]; ?></td>
            </tr>
            <?php
            	$i++;
            }
            ?>
          </tbody>
				</table>
      </div><!-- /.box-body -->
  	</div><!-- /.box -->
	</div>
</div>
  
  
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script type="text/javascript">
$(function() {
	$("#partyName").focus();
	$("#partyName").autocomplete({
      data: [
				<?php
        $sSQL = "SELECT partyId,partyName,phoneNo FROM party ORDER BY partyName";
        $rs = mysql_query($sSQL) or print(mysql_error());
				$addComma = "";
				while($row = mysql_fetch_array($rs)){
					echo $addComma."['".$row["partyName"]."',".$row["partyId"].",".$row["phoneNo"]."]";
					$addComma = ",";
				}
        ?>  
      ],
      minChars: 1,
      onItemSelect: function(item) {
        $("#partyId").val(item.data[0]);
        $("#phoneNo").val(item.data[1]);
      }
  });
});
</script>
<script src="<?php echo $baseUrl.'js/'; ?>trade.js" type="text/javascript"></script>
</body></html>