<?php
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <!--<ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="#">Examples</a></li>
                        <li class="active">Blank page</li>
                    </ol>-->
    
    <h4 class="page-header"> Dashboard <small>Admin Panel</small> </h4>
    <div class="row">
      <!-- left column -->
      <?php //$this->view('admin/msg');?>
      <div class="col-xs-12">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3> 15 </h3>
              <p> User Registrations </p>
            </div>
            <div class="icon"> <i class="ion ion-person-add"></i> </div>
            <a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3> 15 </h3>
              <p> Products </p>
            </div>
            <div class="icon"> <i class="ion ion-ios7-pricetag-outline"></i> </div>
            <a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-blue">
            <div class="inner">
              <h3> 15 </h3>
              <p> Sales </p>
            </div>
            <div class="icon"> <i class="ion ion-ios7-cart-outline"></i> </div>
            <a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a> </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-teal">
            <div class="inner">
              <h3> 14 </h3>
              <p> Notofications </p>
            </div>
            <div class="icon"> <i class="ion ion-ios7-alarm-outline"></i> </div>
            <a href="#" class="small-box-footer"> More info <i class="fa fa-arrow-circle-right"></i> </a> </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Main content -->
  <section class="content"></section>
  <!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<!-- ./wrapper -->
<?php include_once('includes/jsfiles.php'); ?>
