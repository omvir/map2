<?php
include_once('includes/basepath.php');

if(isset($_REQUEST["tradeId"]))
{//We have not handled : if a trade which has made more smaller parts of old realQty, we can not make realQty as old realQty 
 //... count will be correct, but parts may be more than 1 (if not required, then also
 //... because it was required when trade which is being deleted was inserted)
  $selectQ = "SELECT * FROM trade WHERE tradeId = ".trim($_REQUEST["tradeId"]);
  $selectRes = mysql_query($selectQ);
  $tradeData = mysql_fetch_array($selectRes);
  
  //////////////
  //update firstDateTime, secondDateTime, thirdDateTime (based on firstThing, secondThing, thirdThing)
  //If first  ... move second  To first, third to second
  //If second ... third to second, make third blank
  //If third  ... make third blank
	if($tradeData['tradeFor'] == "Parity")
	{
    $transferSQL = "UPDATE trademeta SET firstDateTime = secondDateTime, firstThing = secondThing "
      . " WHERE parityQtyId = ".trim($_REQUEST["tradeId"])." AND firstThing = 'Parity'";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET secondDateTime = thirdDateTime, secondThing = thirdThing "
      . " WHERE parityQtyId = ".trim($_REQUEST["tradeId"])." AND (firstThing = 'Parity' OR secondThing = 'Parity')";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET thirdDateTime = '0000-00-00', thirdThing = ''"
      . " WHERE parityQtyId = ".trim($_REQUEST["tradeId"]);
    mysql_query($transferSQL) or print(mysql_error());//1st, 2nd, 3rd all ... because we have transferred old place to new place and there will be NO 3rd place
  }
	if($tradeData['tradeFor'] == "Rate")
	{
    $transferSQL = "UPDATE trademeta SET firstDateTime = secondDateTime, firstThing = secondThing "
      . " WHERE rateQtyId = ".trim($_REQUEST["tradeId"])." AND firstThing = 'Rate'";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET secondDateTime = thirdDateTime, secondThing = thirdThing "
      . " WHERE rateQtyId = ".trim($_REQUEST["tradeId"])." AND (firstThing = 'Rate' OR secondThing = 'Rate')";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET thirdDateTime = '0000-00-00', thirdThing = ''"
      . " WHERE rateQtyId = ".trim($_REQUEST["tradeId"]);
    mysql_query($transferSQL) or print(mysql_error());//1st, 2nd, 3rd all ... because we have transferred old place to new place and there will be NO 3rd place
  }
	if($tradeData['tradeFor'] == "Bill")
	{
    $transferSQL = "UPDATE trademeta SET firstDateTime = secondDateTime, firstThing = secondThing "
      . " WHERE billQtyId = ".trim($_REQUEST["tradeId"])." AND firstThing = 'Bill'";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET secondDateTime = thirdDateTime, secondThing = thirdThing "
      . " WHERE billQtyId = ".trim($_REQUEST["tradeId"])." AND (firstThing = 'Bill' OR secondThing = 'Bill')";
    mysql_query($transferSQL) or print(mysql_error());
    $transferSQL = "UPDATE trademeta SET thirdDateTime = '0000-00-00', thirdThing = ''"
      . " WHERE billQtyId = ".trim($_REQUEST["tradeId"]);
    mysql_query($transferSQL) or print(mysql_error());//1st, 2nd, 3rd all ... because we have transferred old place to new place and there will be NO 3rd place
	}
  //////////////

  $sSQL2 = "UPDATE trademeta SET parityQtyId = 0, parityQty = 0, parityDate = '0000-00-00', sourceParityQty = 0, parity = 0 "
    . " WHERE parityQtyId = ".trim($_REQUEST["tradeId"]);
  mysql_query($sSQL2) or print(mysql_error());

  $sSQL2 = "UPDATE trademeta SET rateQtyId = 0, rateQty = 0, rateDate = '0000-00-00', sourceRateQty = 0, rate = 0 "
    . " WHERE rateQtyId = ".trim($_REQUEST["tradeId"]);
  mysql_query($sSQL2) or print(mysql_error());

  $sSQL2 = "UPDATE trademeta SET billQtyId = 0, billQty = 0, billDate = '0000-00-00', sourceBillQty = 0, vat = 0 "
    . " WHERE billQtyId = ".trim($_REQUEST["tradeId"]);
  mysql_query($sSQL2) or print(mysql_error());

  $sSQL = "DELETE FROM trademeta"
    ." WHERE parityQtyId = 0"
    ." AND rateQtyId = 0"
    ." AND billQtyId = 0";
  mysql_query($sSQL) or print(mysql_error());

  $sSQL = "DELETE FROM trade WHERE tradeId = ".trim($_REQUEST["tradeId"]);
  mysql_query($sSQL) or print(mysql_error());
  
  header("Location:trade.php");
  exit;
}
?>