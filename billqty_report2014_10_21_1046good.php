<?php
include_once('includes/header.php');
include_once('includes/topheader.php');
include_once('includes/leftside.php');
?>
<style type="text/css">
.color-entry .form-group { margin-bottom:8px; }
.color-entry .form-control { height:30px; padding:4px 12px;}
.color-entry textarea.form-control { height:auto;}
.color-entry .table > tbody > tr > td { padding:5px 8px; vertical-align:middle; }
.fltLeft label { float:left; width:100px; line-height:25px; }
.fltLeft .form-control { width:70%; }
</style>
<aside class="right-side">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1> Item</h1>
</section>
<section class="content color-entry">
  <div class="col-xs-10">
    		<div class="box">
        	<div class="box-body table-responsive">
            <table class="table table-bordered table-hover">
            	<thead>
              	<tr>
                  <th>Date</th>
                  <th>Bill Qty</th>
                  <th>Real Qty</th>
                  <th>Parity Qty</th>
                  <th>Rate Qty</th>
                  <th>Parity</th>
                  <th>Rate</th>
                  <th>RateAfterParity</th>
                </tr>
              </thead>
              <tbody>
				<?php
        $totalBillQty      = 0;
        $billQtyWithParity = 0;
        $billQtyWithRate   = 0;
				echo "<Br />".$qrySelTradeMeta = "SELECT *, SUM(billQty) as totalBillQty FROM trademeta "
          . " GROUP BY billQtyId HAVING billQtyId > 0 "
          . " ORDER BY firstDateTime ASC, secondDateTime ASC, thirdDateTime ASC";
				$resSelTradeMeta = mysql_query($qrySelTradeMeta);
				if(mysql_num_rows($resSelTradeMeta)>0)
				{
          $billRowCount = 0;
					while($qFetchTradeMeta = mysql_fetch_array($resSelTradeMeta))
					{
					  echo "<Br /><Br /><Br />";print_r($qFetchTradeMeta);
            $billRowCount++;
            if($billRowCount % 2 == 0)
              $rowColor = "";
            else
              $rowColor = "pink";
            $totalBillQty      += $qFetchTradeMeta['totalBillQty'];
            $billQtyWithParity += $qFetchTradeMeta['parityQty'];
            $billQtyWithRate   += $qFetchTradeMeta['rateQty'];
						?>
						<tr style="background-color: <?php echo $rowColor; ?>">
							<td NOWRAP>
                <?php 
                  echo date('d/m/Y',strtotime($qFetchTradeMeta["billDate"])); 
                  echo "<br />".$qFetchTradeMeta["firstDateTime"]." ".$qFetchTradeMeta["firstThing"]; 
                  echo "<br />".$qFetchTradeMeta["secondDateTime"]." ".$qFetchTradeMeta["secondThing"]; 
                  echo "<br />".$qFetchTradeMeta["thirdDateTime"]." ".$qFetchTradeMeta["thirdThing"]; 
                ?>
              </td>
							<td align="right"><?php echo $qFetchTradeMeta["totalBillQty"]; ?></td>
							<td align="right"><?php echo $qFetchTradeMeta["realQty"]; ?></td>
							<td align="right">
                <?php 
                  if($qFetchTradeMeta["parityQty"] != 0)
                    echo $qFetchTradeMeta["parityDate"]." => ".$qFetchTradeMeta["parityQty"]." / ".$qFetchTradeMeta["sourceParityQty"]; 
                  else
                    echo "&nbsp;";
                ?>
              </td>
							<td align="right">
                <?php 
                  if($qFetchTradeMeta["rateQty"] != 0)
                    echo $qFetchTradeMeta["rateDate"]." => ".$qFetchTradeMeta["rateQty"]." / ".$qFetchTradeMeta["sourceRateQty"]; 
                  else
                    echo "&nbsp;";
                ?>
              </td>
							<td align="right"><?php echo $qFetchTradeMeta["parity"]; ?></td>
							<td align="right"><?php echo $qFetchTradeMeta["rate"]."<br />Amount: ".($qFetchTradeMeta["rate"]*$qFetchTradeMeta["billQty"]); ?></td>
							<td align="right">
                <?php 
                  if($qFetchTradeMeta["rate"] != 0)
                  {
                    echo $qFetchTradeMeta["rate"] + $qFetchTradeMeta["parity"]."<br />".($qFetchTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchTradeMeta["billQty"]; 
                  }
                  else
                  {
                    echo 0;
                  }
                ?>
              </td>
						</tr>
						<?php
						echo "<Br />".$qrySelRemainTradeMeta = "SELECT * FROM trademeta "
              . " WHERE tradeMetaId!='".$qFetchTradeMeta["tradeMetaId"]."' "
              . " AND billQtyId='".$qFetchTradeMeta["billQtyId"]."'"
              . " ORDER BY firstDateTime ASC, secondDateTime ASC, thirdDateTime ASC";
						$resSelRemainTradeMeta = mysql_query($qrySelRemainTradeMeta);
						if(mysql_num_rows($resSelRemainTradeMeta)>0)
						{
							while($qFetchRemainTradeMeta = mysql_fetch_array($resSelRemainTradeMeta))
							{
                $billQtyWithParity += $qFetchRemainTradeMeta['parityQty'];
                $billQtyWithRate   += $qFetchRemainTradeMeta['rateQty'];
								?>
								<tr style="background-color: <?php echo $rowColor; ?>">
                  <td nowrap>
                    <?php
                      echo "<br />".$qFetchRemainTradeMeta["firstDateTime"]." ".$qFetchRemainTradeMeta["firstThing"];
                      echo "<br />".$qFetchRemainTradeMeta["secondDateTime"]." ".$qFetchRemainTradeMeta["secondThing"];
                      echo "<br />".$qFetchRemainTradeMeta["thirdDateTime"]." ".$qFetchRemainTradeMeta["thirdThing"];
                    ?>
                  </td>
									<td>&nbsp;</td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["realQty"]; ?></td>
									<td align="right">
                    <?php 
                      if($qFetchRemainTradeMeta["parityQty"] != 0)
                        echo $qFetchRemainTradeMeta["parityDate"]." => ".$qFetchRemainTradeMeta["parityQty"]." / ".$qFetchRemainTradeMeta["sourceParityQty"]; 
                      else
                        echo "&nbsp;";
                    ?>
                  </td>
									<td align="right">
                    <?php 
                      if($qFetchRemainTradeMeta["rateQty"] != 0)
                        echo $qFetchRemainTradeMeta["rateDate"]." => ".$qFetchRemainTradeMeta["rateQty"]." / ".$qFetchTradeMeta["sourceRateQty"]; 
                      else
                        echo "&nbsp;";
                    ?>
                  </td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["parity"]; ?></td>
									<td align="right"><?php echo $qFetchRemainTradeMeta["rate"]."<br />Amount: ".($qFetchRemainTradeMeta["rate"]*$qFetchRemainTradeMeta["billQty"]); ?></td>
									<td align="right">
                    <?php
                      if($qFetchRemainTradeMeta["rate"] != 0)
                      {
                        echo $qFetchRemainTradeMeta["rate"] + $qFetchTradeMeta["parity"]."<br />".(($qFetchRemainTradeMeta["rate"] + $qFetchTradeMeta["parity"])*$qFetchRemainTradeMeta["billQty"]); 
                      }
                      else
                      {
                        echo 0;
                      }
                    ?>
                  </td>
								</tr>
								<?php
							}
						}
					}
				}
				else
				{
					?>
					<tr>
						<th colspan="10">No Records Found</th>
					</tr>
					<?php
				}
                ?>
              </tbody>
            </table>
            <table border="1" cellpadding="4">
              <tr align="right">
                <td>Total Bill Qty</td>
                <td>Parity Updated For Bill</td>
                <td>Rate Updated For Bill</td>
                <td>Pending Parity For Bill</td>
                <td>Pending Rates For Bill</td>
                <td>Pending Rates For Parity</td>
              </tr>
              <tr align="right">
                <td><?php echo $totalBillQty; ?></td>
                <td><?php echo $billQtyWithParity; ?></td>
                <td><?php echo $billQtyWithRate; ?></td>
                <td><?php echo ($totalBillQty - $billQtyWithParity); ?></td>
                <td><?php echo ($totalBillQty - $billQtyWithRate); ?></td>
                <td><?php echo ($billQtyWithParity - $billQtyWithRate); ?></td>
              </tr>
            </table>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div>
  
  </div>
  
  
</section>
<!-- /.content -->
</aside>
<!-- /.right-side -->
</div>
<?php include_once('includes/jsfiles.php'); ?>
<script src="<?php echo $baseUrl.'js/'; ?>item.js" type="text/javascript"></script>
</body></html>