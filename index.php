<?php 
	include_once('includes/header.php');
?>
<body class="bg-black">
<div class="form-box" id="login-box">
  <?php include_once('msg.php');?>
  <div class="header">Admin Panel</div>
  <form action="dashboard.php" method="post">
    <div class="body bg-gray">
      <div class="form-group">
        <input type="password" name="pwd" class="form-control" placeholder="Password"/>
      </div>
      <!--<div class="form-group">
                        <input type="checkbox" name="remember_me"/> Remember me
                    </div>-->
    </div>
    <div class="footer">
      <button type="submit" class="btn bg-olive btn-block">Login</button>
      <!--<p><a href="#">I forgot my password</a></p>-->
      <!--<a href="register.html" class="text-center">Register a new membership</a>-->
    </div>
  </form>
</div>
<?php include_once('includes/jsfiles.php'); ?>
