-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 06, 2014 at 09:27 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `map2`
--

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE IF NOT EXISTS `item` (
`itemId` int(11) NOT NULL,
  `itemName` varchar(255) DEFAULT NULL,
  `multiply` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`itemId`, `itemName`, `multiply`) VALUES
(1, 'Gold', 10),
(2, 'Sliver', 1);

-- --------------------------------------------------------

--
-- Table structure for table `party`
--

CREATE TABLE IF NOT EXISTS `party` (
`partyId` int(11) NOT NULL,
  `partyName` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(15) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `party`
--

INSERT INTO `party` (`partyId`, `partyName`, `phoneNo`) VALUES
(1, 'ABC', '991111111');

-- --------------------------------------------------------

--
-- Table structure for table `trade`
--

CREATE TABLE IF NOT EXISTS `trade` (
`tradeId` int(11) NOT NULL,
  `tradeDate` date NOT NULL,
  `partyId` int(11) NOT NULL DEFAULT '0',
  `itemId` int(11) NOT NULL,
  `tranType` varchar(4) NOT NULL DEFAULT 'BUY',
  `qty` double DEFAULT NULL,
  `parityUsedQty` double DEFAULT NULL,
  `rateUsedQty` double DEFAULT NULL,
  `billUsedQty` double NOT NULL DEFAULT '0',
  `parity` double DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `bill` enum('N','Y') NOT NULL DEFAULT 'N',
  `vat` double DEFAULT NULL,
  `unfixRate` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trademeta`
--

CREATE TABLE IF NOT EXISTS `trademeta` (
`tradeMetaId` int(11) NOT NULL,
  `firstCreatedAt` datetime DEFAULT NULL,
  `partyId` int(11) NOT NULL,
  `realQty` double NOT NULL,
  `parityQtyId` int(11) DEFAULT '0',
  `rateQtyId` int(11) NOT NULL DEFAULT '0',
  `billQtyId` int(11) DEFAULT '0',
  `parityDate` date DEFAULT NULL,
  `rateDate` date DEFAULT NULL,
  `billDate` date NOT NULL,
  `parityQty` double NOT NULL,
  `rateQty` double NOT NULL,
  `billQty` double NOT NULL,
  `parity` double NOT NULL,
  `rate` double NOT NULL,
  `vat` double NOT NULL DEFAULT '0',
  `itemId` int(11) DEFAULT NULL,
  `tranType` varchar(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `item`
--
ALTER TABLE `item`
 ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `party`
--
ALTER TABLE `party`
 ADD PRIMARY KEY (`partyId`);

--
-- Indexes for table `trade`
--
ALTER TABLE `trade`
 ADD PRIMARY KEY (`tradeId`);

--
-- Indexes for table `trademeta`
--
ALTER TABLE `trademeta`
 ADD PRIMARY KEY (`tradeMetaId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `party`
--
ALTER TABLE `party`
MODIFY `partyId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `trade`
--
ALTER TABLE `trade`
MODIFY `tradeId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trademeta`
--
ALTER TABLE `trademeta`
MODIFY `tradeMetaId` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
