ALTER TABLE `trademeta` CHANGE `firstCreatedAt` `firstDateTime` DATETIME NULL DEFAULT NULL;

ALTER TABLE `trademeta` ADD `secondDateTime` DATETIME NULL AFTER `firstDateTime`, ADD `thirdDateTime` DATETIME NULL AFTER `secondDateTime`;

ALTER TABLE `trademeta` CHANGE `firstDateTime` `firstDateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00', CHANGE `secondDateTime` `secondDateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00', CHANGE `thirdDateTime` `thirdDateTime` DATETIME NULL DEFAULT '0000-00-00 00:00:00';

ALTER TABLE `trademeta` CHANGE `partyId` `partyId` INT(11) NULL DEFAULT '0', CHANGE `realQty` `realQty` DOUBLE NULL DEFAULT '0', CHANGE `rateQtyId` `rateQtyId` INT(11) NULL DEFAULT '0', CHANGE `parityDate` `parityDate` DATE NULL DEFAULT '0000-00-00', CHANGE `rateDate` `rateDate` DATE NULL DEFAULT '0000-00-00', CHANGE `billDate` `billDate` DATE NULL DEFAULT '0000-00-00', CHANGE `parityQty` `parityQty` DOUBLE NULL DEFAULT '0', CHANGE `rateQty` `rateQty` DOUBLE NULL DEFAULT '0', CHANGE `billQty` `billQty` DOUBLE NULL DEFAULT '0', CHANGE `itemId` `itemId` INT(11) NULL DEFAULT '0';